<?php

return array(
	'router' => array(
		'routes' => array(
			'album' => array(
				'type'    => 'Segment',
				'options' => array(
					'route'    => '/album[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'     => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Album\Controller\Album',
						'action'     => 'index',
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Album\Controller\Album' => \Album\Controller\AlbumController::class,
		),
	),
	'service_manager' => array(
		'factories' => array(
			\Album\Model\AlbumTable::class =>  \Album\Model\Factory\AlbumTableFactory::class,
			'AlbumTableGateway' => \Album\Model\Factory\AlbumTableGatewayFactory::class,
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'album' => __DIR__ . '/../view',
		),
	),
);