<?php
/**
 * Created by PhpStorm.
 * User: rafaellabegalini
 * Date: 8/9/15
 * Time: 9:10 PM
 */

namespace Album\Model\Factory;


use Album\Model\Album;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AlbumTableGatewayFactory implements FactoryInterface
{
	/**
	 * Create service
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return mixed
	 */
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');

		$resultSetPrototype = new ResultSet();
		$resultSetPrototype->setArrayObjectPrototype(new Album());

		return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
	}

}