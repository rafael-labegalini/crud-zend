<?php
/**
 * Created by PhpStorm.
 * User: rafaellabegalini
 * Date: 8/9/15
 * Time: 9:09 PM
 */

namespace Album\Model\Factory;


use Album\Model\AlbumTable;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AlbumTableFactory implements FactoryInterface
{
	/**
	 * Create service
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return mixed
	 */
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$tableGateway = $serviceLocator->get('AlbumTableGateway');
		$table = new AlbumTable($tableGateway);

		return $table;
	}

}