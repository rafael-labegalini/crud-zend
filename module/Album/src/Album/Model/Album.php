<?php
/**
 * Created by PhpStorm.
 * User: rafaellabegalini
 * Date: 8/9/15
 * Time: 8:58 PM
 */

namespace Album\Model;

use Zend\Stdlib\ArraySerializableInterface;

class Album implements ArraySerializableInterface
{
	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var string
	 */
	public $artist;

	/**
	 * @var string
	 */
	public $title;

	public function exchangeArray(array $data)
	{
		$this->id     = (!empty($data['id'])) ? $data['id'] : null;
		$this->artist = (!empty($data['artist'])) ? $data['artist'] : null;
		$this->title  = (!empty($data['title'])) ? $data['title'] : null;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}