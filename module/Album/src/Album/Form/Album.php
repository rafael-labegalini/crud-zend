<?php
/**
 * Created by PhpStorm.
 * User: rafaellabegalini
 * Date: 8/9/15
 * Time: 10:36 PM
 */

namespace Album\Form;


use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ObjectProperty;

class Album extends Form implements InputFilterProviderInterface
{
	public function __construct()
	{
		parent::__construct('album-form');

		$this->setHydrator(new ObjectProperty())
			->setObject(new \Album\Model\Album());

		$this->add(array(
			'name' => 'id',
			'type' => 'Hidden',
		));

		$this->add(array(
			'name' => 'title',
			'type' => 'Text',
			'options' => array(
				'label' => 'Title',
			),
		));

		$this->add(array(
			'name' => 'artist',
			'type' => 'Text',
			'options' => array(
				'label' => 'Artist',
			),
		));

		$this->add(array(
			'name' => 'submit',
			'type' => 'Submit',
			'attributes' => array(
				'value' => 'Go',
				'id' => 'submitbutton',
			),
		));
	}

	/**
	 * Should return an array specification compatible with
	 * {@link Zend\InputFilter\Factory::createInputFilter()}.
	 *
	 * @return array
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			),
			'artist' => array(
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			),
			'title' => array(
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			),
		);
	}

}