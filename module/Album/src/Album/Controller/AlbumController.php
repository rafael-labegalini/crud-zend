<?php

namespace Album\Controller;


use Album\Model\AlbumTable;
use Album\Form\Album;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AlbumController extends AbstractActionController
{
	/**
	 * @var AlbumTable
	 */
	protected $albumTable = null;

	public function indexAction()
	{
		return new ViewModel(array(
			'albums' => $this->getAlbumTable()->fetchAll(),
		));
	}

	public function addAction()
	{
		$form = new Album();
		$form->get('submit')->setValue('Add');

		$album = new \Album\Model\Album();
		$form->bind($album);

		$request = $this->getRequest(); /* @var $request \Zend\Http\PhpEnvironment\Request */
		if ($request->isPost()) {
			$form->setData($request->getPost());

			if ($form->isValid()) {
				$this->getAlbumTable()->saveAlbum($album);

				// Redirect to list of albums
				return $this->redirect()->toRoute('album');
			}
		}

		return new ViewModel(array('form' => $form));
	}

	public function editAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('album', array(
				'action' => 'add'
			));
		}

		// Get the Album with the specified id.  An exception is thrown
		// if it cannot be found, in which case go to the index page.
		try {
			$album = $this->getAlbumTable()->getAlbum($id);
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('album', array(
				'action' => 'index'
			));
		}

		$form  = new Album();
		$form->bind($album);
		$form->get('submit')->setAttribute('value', 'Edit');

		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setData($request->getPost());

			if ($form->isValid()) {
				$this->getAlbumTable()->saveAlbum($album);

				// Redirect to list of albums
				return $this->redirect()->toRoute('album');
			}
		}

		return new ViewModel(array(
			'id' => $id,
			'form' => $form,
		));
	}

	public function deleteAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('album');
		}

		$request = $this->getRequest();
		if ($request->isPost()) {
			$del = $request->getPost('del', 'No');

			if ($del == 'Yes') {
				$id = (int) $request->getPost('id');
				$this->getAlbumTable()->deleteAlbum($id);
			}

			// Redirect to list of albums
			return $this->redirect()->toRoute('album');
		}

		return new ViewModel(array(
			'id'    => $id,
			'album' => $this->getAlbumTable()->getAlbum($id)
		));
	}

	public function getAlbumTable()
	{
		if (!$this->albumTable) {
			$sm = $this->getServiceLocator();
			$this->albumTable = $sm->get(AlbumTable::class);
		}
		return $this->albumTable;
	}
}